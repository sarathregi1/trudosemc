import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {createStore, applyMiddleware, compose} from 'redux';
import allReducers from './redux/reducers/index';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

const store = createStore(
    allReducers,
    applyMiddleware(thunk)
    // compose(, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()),
);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
, document.getElementById('app'));
