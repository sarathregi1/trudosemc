import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { PlayArrow, Replay10, Forward10 } from '@material-ui/icons';
import { BottomNavigation, BottomNavigationAction, Toolbar, Button, Grid, Paper, Fab} from '@material-ui/core';
import Flexbox from 'flexbox-react';
import ReactPlayer from 'react-player';
import QRCode from 'qrcode.react';
import picture from '../public/Group7.png'

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        position: 'absolute',
        width: "100vw",
        height: "100vh",
        backgroundColor: "#FFF"
    },
    modalstyle: {
        width: "100vw"
    }
});


export default class MobileDeviceView extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }


    render() {
        const { classes } = this.props;
        return (
            <Flexbox flexDirection="column" height="100%" width="100%">
                <Flexbox display="flex" height='100%' flexGrow={1}>
                    <div element="content" style={{overflowX: "none", width: "100%", height: "100%"}}>
                        <Grid container direction="row" style={{height: "100%"}}>
                            <Grid item xs={9} style={{height: "100%", alignSelf: "center"}}>
                                <ReactPlayer url='https://youtu.be/ktT3zsx-lzo' width="100%" height="100%" playing />
                            </Grid>
                            <Grid item xs={3}>
                                <Paper square elevation={5} style={{height: "100%", width: "100%", margin: 0}}>
                                    <Flexbox flexDirection="column" height="100%">
                                        <Flexbox element="header" height="180px" style={{textAlign: "center"}}>
                                            <div style={{width: "100%"}}>
                                                <h1 style={{fontFamily: "lato", textTransform: "uppercase", fontWeight: 200}}>Continue Watching...</h1>
                                            </div>
                                        </Flexbox>
                                        <Flexbox display="flex" height="100%" flexGrow={1}>
                                            <div element="content" style={{height: "100%", width: "100%", margin: 0}}>
                                                <Grid container alignContent="center" alignItems="center" style={{width: "100%", height: "100%"}}>
                                                    <Grid item xs={3}>

                                                    </Grid>
                                                    <Grid item xs={6} style={{textAlign: "center", width: "100%"}}>
                                                        <Grid container alignContent="center" justify="center" style={{width: "100%", height: "100%"}}>
                                                            <Grid item style={{width: "100%"}}>
                                                                {/* // <Paper elevation={3} style={{ width: 150, height: 150}}> */}
                                                                    <QRCode style={{padding: 10}} renderAs="svg" value="http://facebook.github.io/react/" />
                                                                {/* // </Paper> */}
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                    <Grid item xs={3}>

                                                    </Grid>
                                                </Grid>
                                            </div>
                                        </Flexbox>
                                        <Flexbox element="footer" height="180px" style={{textAlign: "center"}}>
                                            <div style={{width: "100%"}}>
                                                <img src={picture} style={{width: "80%"}}></img>
                                            </div>
                                        </Flexbox>
                                    </Flexbox>
                                </Paper>
                            </Grid>
                        </Grid>
                    </div>
                </Flexbox>
            </Flexbox>
        );
    }
}

// CreateAppointmentModal.propTypes = {
//     classes: PropTypes.object.isRequired,
// };

// const mapDispatchToProps = dispatch => ({
//     createPatientAppointment: userInfo => dispatch(createPatientAppointment(userInfo))
// });

// function mapStateToProps(state) {
//     return { allLocationPatients: state.allLocationPatients };
// }

// const styledCompenent = withStyles(styles)(CreateAppointmentModal);
  
// export default connect(mapStateToProps, mapDispatchToProps)(styledCompenent);