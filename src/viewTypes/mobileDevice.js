import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { PlayArrow, Replay10, Forward10, Pause } from '@material-ui/icons';
import { BottomNavigation, BottomNavigationAction, Toolbar, Button, Grid, Paper, Fab} from '@material-ui/core';
import Flexbox from 'flexbox-react';
import {crViewVideoPlayPause, crViewVideoLinkSet} from '../redux/actions/index';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {connect} from 'react-redux';
import TimeFormat from 'hh-mm-ss';
import Moment from 'react-moment';
import moment from 'moment';

const styles = theme => ({
    
});


class MobileDeviceView extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(){
        this.props.crViewVideoPlayPause(true);
    }

    handlePauseClick(){
        this.props.crViewVideoPlayPause(false);
    }

    handleLinkClick(data){
        this.props.crViewVideoLinkSet(data);
        this.props.crViewVideoPlayPause(true);
    }


    render() {
        const { classes } = this.props;
        var time2 = Math.trunc( this.props.crViewProgress.playedSeconds )
        var time3 = Math.trunc( this.props.crViewDuration )
        console.log(time2)
        var time = moment.duration(time2, 'seconds').format("mm:ss");
        var duration = moment.duration(time3, 'seconds').format("mm:ss");
        return (
            <Flexbox flexDirection="column" height='100%' width="100%">
                <Flexbox display="flex" height='100%' flexGrow={1}>
                    <div element="content" style={{overflowX: "none", width: "100%", height: "100%"}}>
                        <Flexbox flexDirection="column" height="100%">
                            <Flexbox element="header" height="200px">
                                <Paper square elevation={3} style={{height: "100%", width: "100%"}}>
                                    <Grid container alignContent="center" justify="center" style={{width: "100%", height: "100%"}}>
                                        <Grid item>
                                            <p style={{fontFamily: "lato", fontWeight: 200, textTransform: "uppercase"}}>
                                            {(this.props.crViewLink != "") ? 
                                                (
                                                    <p style={{fontSize: 20}}>
                                                        <Moment parse="mm:ss" format="mm:ss">
                                                        {time}
                                                    </Moment> / <Moment parse="mm:ss" format="mm:ss">
                                                        {duration}
                                                    </Moment>
                                                    </p>
                                                    
                                                ):(
                                                    null
                                                )
                                            }
                                                
                                            </p>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </Flexbox>
                            <Flexbox display="flex" height="calc(100% - 200px)" flexGrow={1}>
                                <div element="content" style={{overflowX: "hidden", width: "100%", height: "100%", padding: 20}}>
                                    <ExpansionPanel defaultExpanded>
                                        <ExpansionPanelSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1bh-content"
                                        id="panel1bh-header"
                                        >
                                        <p style={{fontFamily: "lato", margin: 0,fontWeight: 200, textTransform: "uppercase"}}>Question 1</p>
                                        <PlayArrow onClick={() => this.handleLinkClick({link: "https://youtu.be/ktT3zsx-lzo"})}/>
                                        </ExpansionPanelSummary>
                                        <ExpansionPanelDetails>
                                        <p style={{fontFamily: "lato",margin: 0, fontWeight: 200, textTransform: "uppercase"}}>
                                            What is TruDose?
                                        </p>
                                        </ExpansionPanelDetails>
                                    </ExpansionPanel>
                                    <ExpansionPanel defaultExpanded>
                                        <ExpansionPanelSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1bh-content"
                                        id="panel1bh-header"
                                        >
                                        <p style={{fontFamily: "lato", margin: 0,fontWeight: 200, textTransform: "uppercase"}}>Question 2</p>
                                        <PlayArrow onClick={() => this.handleLinkClick({link: "https://youtu.be/ZwEEdXe5ob8"})}/>
                                        </ExpansionPanelSummary>
                                        <ExpansionPanelDetails>
                                        <p style={{fontFamily: "lato",margin: 0, fontWeight: 200, textTransform: "uppercase"}}>
                                            Who Will I be treated by?
                                        </p>
                                        </ExpansionPanelDetails>
                                    </ExpansionPanel>
                                    <ExpansionPanel defaultExpanded>
                                        <ExpansionPanelSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1bh-content"
                                        id="panel1bh-header"
                                        >
                                        <p style={{fontFamily: "lato", margin: 0, fontWeight: 200, textTransform: "uppercase"}}> Question 3</p>
                                        <PlayArrow onClick={() => this.handleLinkClick({link: "https://youtu.be/wDEuFK3qJJQ"})}/>
                                        </ExpansionPanelSummary>
                                        <ExpansionPanelDetails>
                                        <p style={{fontFamily: "lato",margin: 0, fontWeight: 200, textTransform: "uppercase"}}>
                                            What to expect during a procedure?
                                        </p>
                                        </ExpansionPanelDetails>
                                    </ExpansionPanel>
                                </div>
                            </Flexbox>
                        </Flexbox>
                    </div>
                </Flexbox>
                <Flexbox element="footer" height="120px">
                    <div style={{backgroundColor: "#fff", height: 120, zIndex: 1250, width: "100%"}}>
                        <Paper square elevation={3} style={{padding: 0, height: "100%", width: "100%"}}>
                            <Grid container direction="row" style={{height: "100%"}}>
                                <Grid item xs={12} style={{height: "100%"}}>
                                    <Grid container direction="row" style={{height: "100%", width: "100%"}}> 
                                        <Grid item xs={4} style={{textAlign: "right"}}>
                                            <Grid container alignItems="center" alignContent="center" style={{height: "100%", width: "100%"}}>
                                                <Grid item style={{width: "100%"}}>
                                                    <Button
                                                    variant="outlined"
                                                    style={{borderRadius: "50%", height: 60, width: 60}}
                                                    disabled
                                                    >
                                                        <Replay10/>
                                                    </Button>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={4} style={{textAlign: "center"}}>
                                            <Grid container alignItems="center" alignContent="center" style={{height: "100%", width: "100%"}}>
                                                <Grid item style={{width: "100%"}}>
                                                    <Button
                                                    variant="outlined"
                                                    style={{borderRadius: "50%", height: 80, width: 80}}
                                                    disabled={this.props.crViewPlayButtonEnable}
                                                    >

                                                        {(this.props.crView == true)?(
                                                            <Pause onClick={() => this.handlePauseClick()} style={{height: 40, width: 40}}/>
                                                        ):(
                                                            <PlayArrow onClick={() => this.handleClick()} style={{height: 40, width: 40}}/>
                                                        )}
                                                    </Button>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={4} style={{textAlign: "left"}}>
                                            <Grid container alignItems="center" alignContent="center" style={{height: "100%", width: "100%"}}>
                                                <Grid item style={{width: "100%"}}>
                                                    <Button
                                                    variant="outlined"
                                                    style={{borderRadius: "50%", height: 60, width: 60}}
                                                    disabled
                                                    >
                                                        <Forward10/>
                                                    </Button>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Paper>
                    </div>
                </Flexbox>
            </Flexbox>
        );
    }
}

MobileDeviceView.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => ({
    crViewVideoPlayPause: userInfo => dispatch(crViewVideoPlayPause(userInfo)),
    crViewVideoLinkSet: userInfo => dispatch(crViewVideoLinkSet(userInfo))
});

function mapStateToProps(state) {
    return { crView: state.crView.crViewPlay, crViewLink: state.crView.playVideoLink, crViewPlayButtonEnable: state.crView.crViewPlayButtonEnable, crViewDuration: state.crView.durationSeconds, crViewProgress: state.crView.progressSeconds };
}

const styledCompenent = withStyles(styles)(MobileDeviceView);
  
export default connect(mapStateToProps, mapDispatchToProps)(styledCompenent);