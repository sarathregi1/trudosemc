import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { PlayArrow, Replay10, Forward10 } from '@material-ui/icons';
import { BottomNavigation, BottomNavigationAction, Toolbar, Button, Grid, Paper, Fab} from '@material-ui/core';
import Flexbox from 'flexbox-react';
import ReactPlayer from 'react-player';
import QRCode from 'qrcode.react';
import {connect} from 'react-redux';
import { crViewDuration, crViewProgress } from '../redux/actions/index';
import picture from '../public/Group7.png'


const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        position: 'absolute',
        width: "100vw",
        height: "100vh",
        backgroundColor: "#FFF"
    },
    modalstyle: {
        width: "100vw"
    }
});


class MobileDeviceView extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
        this.ref = this.ref.bind(this)
        this.duration = this.handleDuration.bind(this)
    }

    ref(player) {
        this.player = player
    }

    handleDuration(data) {
        this.props.crViewDuration(data)
    }

    handleProgress(data) {
        this.props.crViewProgress(data)
    }


    render() {
        const { classes } = this.props;
        return (
            <Flexbox flexDirection="column" height='100%' width="100%">
                <Flexbox display="flex" height='100%' flexGrow={1}>
                    <div element="content" style={{overflowX: "none", width: "100%", height: "100%"}}>
                        <Grid container direction="row" style={{height: "100%"}}>
                            <Grid item xs={9} style={{height: "100%", alignSelf: "center"}}>
                                {(this.props.crViewLink != "") ? 
                                    (
                                        
                                        <ReactPlayer onProgress={(progress) => this.handleProgress(progress)} onDuration={(duration) => this.handleDuration(duration)} url={this.props.crViewLink} width="100%" height="100%" playing={this.props.crViewPlay} ref={this.ref} controls={false} />
                                        
                                    ):(
                                       <Grid container alignContent="center" justify="center" style={{width: "100%", height: "100%"}}>
                                            <Grid item>
                                                <p style={{fontFamily: "lato", fontWeight: 200, fontSize: 20, textTransform: "uppercase", textAlign: "centers"}}>Select a QUestion to view from your Device</p>
                                            </Grid>
                                            <Grid item>
                                                <div style={{width: "100%", textAlign: "center"}}>
                                                    <img src={picture} style={{width: "40%"}}></img>
                                                </div>
                                            </Grid>
                                        </Grid>
                                    )
                                }
                                
                            </Grid>
                            <Grid item xs={3}>
                                <Paper square elevation={5} style={{height: "100%", width: "100%"}}>
                                    <Flexbox flexDirection="column" height="100%">
                                        <Flexbox element="header" height="60px" style={{textAlign: "left", padding: 10}}>
                                            <div style={{width: "100%"}}>
                                                <h1 style={{fontFamily: "lato", textTransform: "uppercase", fontWeight: 200}}>Clinic Room #1</h1>
                                                <h3 style={{fontFamily: "lato", textTransform: "uppercase", fontWeight: 200}}> Welcome, John Smith</h3>
                                                <h3 style={{fontFamily: "lato", textTransform: "uppercase", fontWeight: 200}}> You will be treated for</h3>
                                                <ul style={{fontFamily: "lato", textTransform: "uppercase", fontWeight: 200, padding: 20}}>
                                                    <li>Carpal Tunnel</li>
                                                    <li>Knee Sprain</li>
                                                </ul>
                                            </div>
                                        </Flexbox>
                                        <Flexbox display="flex" height="100%" flexGrow={1}>
                                            <div element="content" style={{height: "100%", width: "100%"}}>
                                                <Grid container alignContent="center" alignItems="center" style={{widht: "100%", height: "100%"}}>
                                                    <Grid item style={{textAlign: "center", width: "100%"}}>
                                                        {/* <Paper elevation={3} style={{ width: 150, height: 150, margin: 0}}> */}
                                                            <QRCode style={{padding: 10}} renderAs="svg" value={this.props.crViewLink} />
                                                        {/* </Paper> */}
                                                    </Grid>
                                                </Grid>
                                            </div>
                                        </Flexbox>
                                        <Flexbox element="footer" height="300px" style={{textAlign: "left"}}>
                                            <Grid container style={{width: "100%", padding: 30}}>
                                                <Grid item style={{width: "100%"}}>
                                                    <h3 style={{fontFamily: "lato", textTransform: "uppercase", fontWeight: 400, textAlign: "left", margin: 0}}>
                                                        Instructions
                                                    </h3>
                                                </Grid>
                                                <Grid item style={{width: "100%"}}>
                                                    <ol style={{fontFamily: "lato", textTransform: "uppercase", lineHeight: "1.5rem", fontWeight: 200, padding: 0}}>
                                                        <li>Scan QR code and follow instructions</li>
                                                        <li>Open Web Browser on device</li>
                                                        <li>Go to link and follow instructions</li>
                                                    </ol>
                                                </Grid>
                                            </Grid>
                                            
                                            
                                        </Flexbox>
                                    </Flexbox>
                                </Paper>
                            </Grid>
                        </Grid>
                    </div>
                </Flexbox>
            </Flexbox>
        );
    }
}

// CreateAppointmentModal.propTypes = {
//     classes: PropTypes.object.isRequired,
// };

const mapDispatchToProps = dispatch => ({
    crViewDuration: userInfo => dispatch(crViewDuration(userInfo)),
    crViewProgress: userInfo => dispatch(crViewProgress(userInfo))
});

function mapStateToProps(state) {
    return { crViewPlay: state.crView.crViewPlay, crViewForwardSeek: state.crView.crViewForwardSeek, crViewLink: state.crView.playVideoLink };
}

// const styledCompenent = withStyles(styles)(CreateAppointmentModal);
  
export default connect(mapStateToProps, mapDispatchToProps)(MobileDeviceView);