import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { PlayArrow, Replay10, Forward10, Pause } from '@material-ui/icons';
import { BottomNavigation, BottomNavigationAction, Toolbar, Button, Grid, Paper, Fab} from '@material-ui/core';
import Flexbox from 'flexbox-react';
import {crViewVideoPlayPause, crViewVideoLinkSet} from '../redux/actions/index';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {connect} from 'react-redux';
import TimeFormat from 'hh-mm-ss';
import Moment from 'react-moment';
import moment from 'moment';

const styles = theme => ({
    
});


class MobileDeviceViewLobby extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(){
        this.props.crViewVideoPlayPause(true);
    }

    handlePauseClick(){
        this.props.crViewVideoPlayPause(false);
    }

    handleLinkClick(data){
        this.props.crViewVideoLinkSet(data);
        this.props.crViewVideoPlayPause(true);
    }


    render() {
        const { classes } = this.props;
        var time2 = Math.trunc( this.props.crViewProgress.playedSeconds )
        var time3 = Math.trunc( this.props.crViewDuration )
        console.log(time2)
        var time = moment.duration(time2, 'seconds').format("mm:ss");
        var duration = moment.duration(time3, 'seconds').format("mm:ss");
        return (
            <Flexbox flexDirection="column" height='100%' width="100%">
                <Flexbox display="flex" height='100%' flexGrow={1}>
                    <div element="content" style={{overflowX: "none", width: "100%", height: "100%"}}>
                        <Flexbox flexDirection="column" height="100%">
                            <Flexbox display="flex" height="100%" flexGrow={1}>
                                <div element="content" style={{overflowX: "hidden", width: "100%", height: "100%", padding: 20}}>
                                    <Grid container alignItems="center" justify="center" style={{width: "100%", height: "100%"}}>
                                        <Grid item style={{width: "100%", height: "100%", textAlign: "center"}}>
                                            <p style={{fontFamily: "lato", textTransform: "uppercase", fontWeight: 200}}>Redirect to link</p>
                                        </Grid>
                                    </Grid>
                                </div>
                            </Flexbox>
                        </Flexbox>
                    </div>
                </Flexbox>
            </Flexbox>
        );
    }
}

MobileDeviceViewLobby.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => ({
    crViewVideoPlayPause: userInfo => dispatch(crViewVideoPlayPause(userInfo)),
    crViewVideoLinkSet: userInfo => dispatch(crViewVideoLinkSet(userInfo))
});

function mapStateToProps(state) {
    return { crView: state.crView.crViewPlay, crViewLink: state.crView.playVideoLink, crViewPlayButtonEnable: state.crView.crViewPlayButtonEnable, crViewDuration: state.crView.durationSeconds, crViewProgress: state.crView.progressSeconds };
}

const styledCompenent = withStyles(styles)(MobileDeviceViewLobby);
  
export default connect(mapStateToProps, mapDispatchToProps)(styledCompenent);