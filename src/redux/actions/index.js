export const crViewVideoPlayPause = (obj) => ({
    type: 'crViewPlayChange',
    payload: obj
});

export const crViewVideoLinkSet = (obj) => ({
    type: 'crViewVideoLinkSet',
    payload: obj
});

export const crViewDuration = (obj) => ({
    type: 'crViewDuration',
    payload: obj
});

export const crViewProgress = (obj) => ({
    type: 'crViewProgress',
    payload: obj
});

export const crViewQRCodeChange = (obj) => ({
    type: 'crViewQRCodeChange',
    payload: obj
});