import {combineReducers} from 'redux';
import crView from './crView';


const allReducers = combineReducers({
    crView
})

export default allReducers;