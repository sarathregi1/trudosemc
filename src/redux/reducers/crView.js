const intialState = {
    crViewPlay: false,
    crForwardSeek: '',
    playVideoLink: '',
    crViewPlayButtonEnable: true,
    durationSeconds: 0,
    progressSeconds: 0,
    crViewQRCode: true
}

const allLocationClinicians = (state = intialState, action) => {
    switch(action.type){
        case 'crViewPlayChange':
            return {...state, crViewPlay: action.payload};
        case 'crViewSeekForward':
            return {...state, crViewPlay: action.payload};
        case 'crViewVideoLinkSet':
            return {...state, playVideoLink: action.payload.link, crViewPlayButtonEnable: false};
        case 'crViewDuration':
            return {...state, durationSeconds: action.payload};
        case 'crViewProgress':
            return {...state, progressSeconds: action.payload};
        case 'crViewQRCodeChange':
            return {...state, crViewQRCode: action.payload};
        default:
            return state;
    }
}

export default allLocationClinicians;