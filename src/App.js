import React from 'react';
import Flexbox from 'flexbox-react';
import {BrowserRouter as Router, Route, NavLink, HashRouter} from 'react-router-dom';
import MobileDeviceView from './viewTypes/mobileDevice';
import MobileDeviceViewLobby from './viewTypes/mobileDeviceLobby';
import LobbyMonitorView from './viewTypes/lobbyMonitor';
import ClinicMonitorView from './viewTypes/ clinicRoomView';
import { Paper, ButtonGroup, Button, Grid } from '@material-ui/core';
import { crViewQRCodeChange } from './redux/actions/index';
import {connect} from 'react-redux';

class App extends React.Component {
  constructor(props) {
      super(props)
      this.state = {
        view: 1
      }
  }


  render() {
    return (
      <Flexbox flexDirection="column" minHeight="100vh">
        <Flexbox element="header" height="7vh">
          <Paper elevation={3} square style={{backgroundColor: "#AC1717", width: "100%", height: "100%", textAlign: "center"}}>
            <Grid container alignItems="center" justify="center" style={{height: "100%", width: "100%"}}>
              <Grid item>
                <ButtonGroup size="large" aria-label="large outlined primary button group">
                  <Button onClick={() => this.setState({view: 0})} style={{color: "#fff", fontFamily: "lato"}}>Lobby View</Button>
                  <Button onClick={() => this.setState({view: 1})} style={{color: "#fff", fontFamily: "lato"}}>CR View</Button>
                </ButtonGroup>
              </Grid>
            </Grid>
          </Paper>
        </Flexbox>
        <Flexbox display="flex" height="93vh" flexGrow={1}>
            <div element="content" style={{overflowX: "hidden", width: "100%", height: "100%"}}>
                <Grid container direction="row" style={{width: "100%", height: "100%"}}>
                  <Grid item xs={9}>
                    <Flexbox flexDirection="column" height="100%" style={{width: "100%"}}>
                      <Flexbox element="header" height="60px">
                        <div style={{width: "100%", backgroundColor: "blue", color: "#fff", textAlign: "center", fontFamily: "lato", fontWeight: "200", textTransform: "uppercase"}}>
                          { (this.state.view === 0)?(
                            <p>Lobby View</p>
                          ):(null)}
                          { (this.state.view === 1)?(
                            <p>Clinic Room View</p>
                          ):(null)}
                        </div>
                      </Flexbox>
                      <Flexbox display="flex" height="100%" flexGrow={1}>
                          { (this.state.view === 0)?(
                            <LobbyMonitorView/>
                          ):(null)}
                          { (this.state.view === 1)?(
                           <ClinicMonitorView/>
                          ):(null)}
                        
                      </Flexbox>
                    </Flexbox>
                  </Grid>
                  <Grid item xs={3}>
                    <Flexbox flexDirection="column" height="100%">
                      <Flexbox element="header" height="60px">
                        <div style={{width: "100%", backgroundColor: "green", color: "#fff", textAlign: "center", fontFamily: "lato", fontWeight: "200", textTransform: "uppercase"}}>
                          <p>Mobile View</p>
                        </div>
                      </Flexbox>
                      <Flexbox display="flex" height="100%" flexGrow={1}>
                        {(this.props.crViewQRCodeState === true) ? (
                            <Grid container alignItems="center" justify="center" style={{width: "100%", height: "100%"}}>
                              <Grid item>
                                  <Button variant="outlined" onClick={() => this.props.crViewQRCodeChange(false)}>
                                    Scan QRCode
                                  </Button>
                              </Grid>
                            </Grid>
                        ):(
                            <div>
                              {(this.state.view === 1) ? (
                                  <MobileDeviceView/>
                              ):(
                                  <MobileDeviceViewLobby/>
                              )}
                            </div>
                        )}
                      </Flexbox>
                    </Flexbox>
                  </Grid>
                </Grid>
            </div>
        </Flexbox>
      </Flexbox>
    )
  }

}


const mapDispatchToProps = dispatch => ({
  crViewQRCodeChange: userInfo => dispatch(crViewQRCodeChange(userInfo)),
});

function mapStateToProps(state) {
  return { crViewQRCodeState: state.crView.crViewQRCode, crViewLink: state.crView.playVideoLink, crViewPlayButtonEnable: state.crView.crViewPlayButtonEnable, crViewDuration: state.crView.durationSeconds, crViewProgress: state.crView.progressSeconds };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);