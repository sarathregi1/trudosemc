const webpack = require('webpack');

module.exports = {
    // 1
    entry: './src/index.js',
    // 2
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader',
                ],
            },
        ]
      },
      resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    output: {
      path: __dirname + './public',
      publicPath: '/',
      filename: 'bundle.js'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],
    // 3
    devServer: {
      contentBase: './public',
      hot: true,
      port: 3000,
    //   host: "192.168.1.35"
    }
};